import os
from flask import Flask, render_template, request, redirect, url_for
from werkzeug.utils import secure_filename
import numpy as np
import tensorflow as tf
from PIL import Image

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = 'static/uploads'
app.config['ALLOWED_EXTENSIONS'] = {'png', 'jpg', 'jpeg', 'gif'}


model_path = 'model.h5'

# Load the model once at the start
model_pipeline = tf.keras.models.load_model(model_path)

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in app.config['ALLOWED_EXTENSIONS']

def preprocess_image(image_path):
    # Load and preprocess the image
    img = Image.open(image_path)
    img = img.resize((224, 224))  # Resize to match model input size
    img_array = np.array(img)  # Convert PIL Image to NumPy array
    img_array = img_array / 255.0  # Normalize pixel values
    img_array = np.expand_dims(img_array, axis=0)  # Expand dimensions to create batch size of 1
    return img_array

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        if 'file' not in request.files:
            return redirect(request.url)
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            
            # Create the uploads directory if it doesn't exist
            os.makedirs(app.config['UPLOAD_FOLDER'], exist_ok=True)
            
            file.save(file_path)
            return redirect(url_for('display', filename=filename))
    return render_template('upload.html')

@app.route('/display/<filename>')
def display(filename):
    return render_template('display.html', filename=filename)

@app.route('/result/<filename>')
def result(filename):
    img_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    preprocessed_image = preprocess_image(img_path)
    
    # Make prediction
    prediction = model_pipeline.predict(preprocessed_image)
    probability = prediction[0][0]
    
    if probability >= 0.5:
        result_text = f"You have Brain Tumor (Probability: {probability*100: .4f}%)"
    else:
        result_text = f"No, you do not have Brain Tumor (Probability: {probability*100:.4f}%)"
    
    return render_template('result.html', filename=filename, result=result_text)

if __name__ == '__main__':
    app.run(debug=True)
